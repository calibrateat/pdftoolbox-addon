# calibrate Masterprofil - Installation

1. Import der Preflight_v2 kfpx in Callas Toolbox
2. Anlegen eines Verzeichnisses
   * Mac: /Library/Application Support/calibrate
   * Windows: C:\ProgramData\calibrate
3. Kopieren der Datei _Script licenses.txt_ in das calibrate Verzeichnis
4. Kopieren der Ordner Fonts, DL Profiles, Output Intents und ICC Profile in das calibrate Verzeichnis
5. Kopieren der custom.json und templatesChooser.json in das calibrate Verzeichnis
  
## Die custom.json Datei

In dieser Datei stehen speziell definierte Templates, die eigene Prüfungen und Korrekturen enthalten oder die gegenüber den Standard Templates abgeänderte Werte enthalten. Ein Template entspricht einer Kombination an Prüfungen und Korrekturen.

## Die templatesChooser.json Datei

In dieser Datei befinden sich eine Liste an Templates, die in der custom.json detailiert erklärt sind. Ist mehr als ein Template definiert, so erhält man bei der Ausführung der .kfpx Datei eine Auswahlliste.

Mit ausgeliefert ist jeweils eine exemplarische _custom.json_ und _templatesChooser.json_ Datei.

## Update

Beim Updaten einer bestehenden Installation muss der alte Prozessplan gelöscht und die neu .kfpx Datei Importiert werden.

## weitere Dateien

**Excel Liste**: enthält eine Liste mit allen Korrekturen und Prüfungen, sowie die verwendeten Variablen. Auf einem eigenem Blatt sind die Definitionen der standard Templates ersichtlich.
  
**masterprofile_templates.pdf**:
graphische Veranschaulichung der mitgelieferten Tempaltes

Wenn eigene Device Link Profile, ICC Profile, Schriften oder Output Intents verarbeitet werden sollen, müssen diese in den vorgesehenen Ordner im calibrate Verzeichnis platziert werden.
  
## Auslesen der Seriennummer

Ausführen der _Check for SN.kfpx_ und erzeugen eines XML Reports oder eines anderen Reports, in dem auch die detailierte Beschreibung des Hits sichtbar ist. Diese Datei ist zur Anforderung einer Lizenzdatei notwendig.
  
## Verifizieren der Templatedatei custom.json gegen das Schema

Editor: Microsoft Visual Studio Code
Öffnet man den gesamten Ordner _pdftoolbox-addon_ in Code, so wird automatisch eine Settingsdatei geladen. Editiert man nun eine Datei mit dem Namen _custom.json_ oder _addon_templates.json_, so führt Code automatisch eine Validierung durch.